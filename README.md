# stubby
Simple multiplatform docker image for cloudflare DNS-over-TLS using stubby. Based on alpine.

run with:
```bash
docker run --init -p 127.0.0.1:53:53/udp registry.gitlab.com/luckyturtledev/docker-stubby:latest
```

example docker-compose:
```yml
version: "3"

services:
  stubby:
    image: registry.gitlab.com/luckyturtledev/docker-stubby:latest
    volumes:
      - ./stubby.yml:/etc/stubby/stubby.yml:ro
    ports:
      - 53:53/udp 
      - 53:53/tcp
    restart: unless-stopped

```

You can mount your own `stubby.yml` under `/etc/stubby/stubby.yml`.
